<?php

namespace Drupal\entity_usage_integrity\Event;

/**
 * Provides a list of events dispatched by the entity_usage_integrity module.
 */
final class EntityUsageIntegrityEvents {

  /**
   * Name of the event dispatched when integrity check applicability is checked.
   *
   * Allows modules to change the applicability status on a specific form.
   *
   * @see \Drupal\entity_usage_integrity\FormIntegrityValidation\ViewedEditForm::isApplicable()
   * @see \Drupal\entity_usage_integrity\FormIntegrityValidation\ViewedDeleteForm::isApplicable()
   */
  const APPLICABILITY_CHECK = 'entity_usage_integrity.applicability_check';

}
