<?php

namespace Drupal\entity_usage_integrity\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an entity usage integrity applicability check event.
 */
class EntityUsageIntegrityApplicabilityCheckEvent extends Event {

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * The applicability status.
   *
   * @var bool
   */
  protected $isApplicable;

  /**
   * Constructs an applicability check event object.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param bool $is_applicable
   *   The default applicability status. TRUE if an entity usage integrity check
   *   must be performed on the form, FALSE otherwise.
   */
  public function __construct(FormStateInterface $form_state, $is_applicable) {
    $this->formState = $form_state;
    $this->isApplicable = $is_applicable;
  }

  /**
   * Gets the form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state.
   */
  public function getFormState() {
    return $this->formState;
  }

  /**
   * Check if the form is applicable for an integrity check.
   *
   * @return bool
   *   TRUE if an entity usage integrity check must be performed on the form,
   *   FALSE otherwise.
   */
  public function isApplicable() {
    return $this->isApplicable;
  }

  /**
   * Sets whether an entity usage integrity check must be performed on the form.
   *
   * @param bool $is_applicable
   *   TRUE if an entity usage integrity check must be performed on the form,
   *   FALSE otherwise.
   */
  public function setApplicable($is_applicable) {
    $this->isApplicable = $is_applicable;
  }

}
